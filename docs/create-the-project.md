In this section, we will create a new project. 

### Instructions
1. Navigate to TBD to download the export of the [GitLab Project](https://gitlab.com/aleesha-m-dawson/react-ci-tutorial/app/) we are using here. 
2. Navigate to [the new project page](https://gitlab.com/projects/new) and select "Import Project."
3. Select "GitLab Export."
4. Name your project ("React Demo App," or whatver you like) and choose your username as the namespace to create the project under your projects. 
5. Click "Choose File" and select the downloaded export.
6. Click the "Import Project" button.
7. After a few minutes, the project will import and be viewable from the browser
8. At this point, you can clone the application to your local environment, or if you prefer, you can make changes in the WebIDE available through the browser.


### Resources
- [Git Basics - Cloning a Repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
- [GitLab's WebIDE Documentation](https://docs.gitlab.com/ee/user/project/web_ide/)
