In this section, we will leverage the already written unit tests and automate them in the pipeline.

### Instructions
1. Navigate to the project you created. 
2. Navigate to the issues in the project.
3. Click on the "Automate unittests" issue. This should be issue #2. 
4. Create the feature branch:
    1. Click "Create merge request." This will create a feature branch and take to to a new view. 
    2. Assign yourself to the merge request by clicking "Assign to me."
    3. Click "Create merge request." This will take to to the merge request view.  
5. Add the code:
    1. From the merge request view, click on the branch name `2-automate-unittests` to navigate to the repository view of that branch.
    2. Click the Web IDE button to open the browser supported file editor. 
    3. Edit the  `.gitlab-ci.yml` file.
    4. Add the following code to the file: 
    ```{}
    react-scripts-test:
      stage: test
      before_script:
      - npm install
      script:
      - npm run test:ci
    ```
    5. Commit the code by clicking "Create commit..." 
    6. Leave the default choices and click "Commit"
6. Return to the merge request view and "Mark as ready."
7. When the pipeline completes succesfully, click "Merge." 

### Resources
- [Running React Tests](https://create-react-app.dev/docs/running-tests/)

