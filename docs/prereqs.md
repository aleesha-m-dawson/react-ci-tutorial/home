In order to successfully complete this hands-on, self-paced demo, you will need certain software on your machine. 

### Pre-requisities
1. To edit the code locally, you will need a code editor. [Visual Studio Code](https://code.visualstudio.com/) is a popular open-source code editor.
2. Version control software [`git`](https://git-scm.com/downloads) is used to track changes. Visit the link to download `git` if you do not already have it.
3. Node package manager, or [`npm`](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm), is used to run the application we are building here. Visit the link to download `npm` if you do not already have it. 
