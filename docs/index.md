# Overview

This documentation is designed to take a new GitLab user on a journey from idea to production using GitLab CI/CD. We will start with a [project](https://gitlab.com/aleesha-m-dawson/react-ci-tutorial/app) pre-populated with code and issues, create a CI/CD pipeline, and end with a delivered static React application. 

## About the Application

The React application we are using in the demonstration was inspired by the open source project [React Nice Resume](https://github.com/nordicgiant2/react-nice-resume). 

## Resources 

- [GitLab Webinars of YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq)
- [GitLab CI/CD Documenation](https://docs.gitlab.com/ee/ci/)

