In this section, we will use the [GitLab Recommended Process](https://docs.gitlab.com/ee/topics/gitlab_flow.html#issue-tracking-with-gitlab-flow) to use an issue as the starting point of adding a pipeline to the project. We will use the GitLab Web IDE in order to make code changes. 

### Instructions
1. Navigate to the project you just created. 
2. Navigate to the issues in the project.
3. Click on the "Create a build job" issue. This should be issue #1. 
4. Create the feature branch:
    1. Click "Create merge request." This will create a feature branch and take to to a new view. 
    2. Assign yourself to the merge request by clicking "Assign to me."
    3. Click "Create merge request." This will take to to the merge request view.  
5. Add the code:
    1. From the merge request view, click on the branch name `1-create-a-build-job` to navigate to the repository view of that branch.
    2. Click the Web IDE button to open the browser supported file editor. 
    3. Create a new file at the root level of the project named `.gitlab-ci.yml`.
    4. Add the following code to the file: 
    ```{}
    image: node:16.0.0

    stages:
    - build
    - test
    - deploy

    build-react-app:
      stage: build
      before_script:
      - npm install --omit=dev
      script:
      - npm run build
    artifacts:
      paths:
      - build
    ```
    5. Commit the code by clicking "Create commit..." 
    6. Leave the default choices and click "Commit"
6. Return to the merge request view and "Mark as ready."
7. When the pipeline completes succesfully, click "Merge." You have officially added your first merge commit to a `main` branch following the GitLab recommended process! 

### Resources
- [GitLab's WebIDE Documentation](https://docs.gitlab.com/ee/user/project/web_ide/)
- [From Idea to Production: Using Gitlab for Your Whole Development Lifecycle](https://www.youtube.com/watch?v=SSGfCU5FaF4)
- More practice: [Create Your First Pipeline](https://docs.gitlab.com/ee/ci/quick_start/)
