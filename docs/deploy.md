In this section, we will add a job to automate the deployment of our React App to GitLab Pages.

### Instructions
1. Navigate to the project you created. 
2. Navigate to the issues in the project.
3. Click on the "Deploy to pages" issue. This should be issue #3. 
4. Create the feature branch:
    1. Click "Create merge request." This will create a feature branch and take to to a new view. 
    2. Assign yourself to the merge request by clicking "Assign to me."
    3. Click "Create merge request." This will take to to the merge request view.  
5. Add the code:
    1. From the merge request view, click on the branch name `3-deploy-to-pages` to navigate to the repository view of that branch.
    2. Click the Web IDE button to open the browser supported file editor. 
    3. Navigate to the `.gitlab-ci.yml` file.
    4. Add the following code to the file: 
    ```{}
    pages:
      image: alpine:latest
      variables:
        GIT_STRATEGY: none
      script:
      - echo "Deploying to GitLab Pages"
      - mv build public         
      artifacts:
        paths:
        - public
      rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    ```
    5. Commit the code by clicking "Create commit..." 
    6. Leave the default choices and click "Commit".
6. Return to the merge request view and "Mark as ready."
7. When the pipeline completes succesfully, click "Merge." This will kick-off a new pipeline. When the new pipeline completes successfully, your app will be deployed. 
8. Navigate to `https://<USERNAME_OR_NAMESPACE>.gitlab.io/<APP_NAME>/` to see your deployed application!

### Resources
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Deploying a React App to GitLab Pages](https://dev.to/jtorbett23/react-gitlab-pages-42l6)
